use std::env;
use sysinfo::{NetworkExt, NetworksExt, ProcessExt, System, SystemExt, UserExt, ComponentExt};

fn main() {
    println!();
    println!("-------------------------------");
    println!("Sys CLI get info to your System");
    println!("-------------------------------");
    println!();

    //recupérer les arguments
    let args: Vec<String> = env::args().collect();
    let sys = init_sys_info();
    
    //si pas d'arguments
    if args[1..].len() as u8 == 0 {
        println!();
        println!("undetect command check that ⬇⬇");
        println!();
        help();
        return;
    }
    
    //boucle sur les arguments
    for command in &args[1..] {
        let s = String::from(command);
        match s.as_str() {
            "cpu"|"c" => {
                cpu(&sys);
                println!();
                return;
            }
            "memory"|"m" => {
                memory(&sys);
                println!();
                return;
            }
            "uptime"|"up" => {
                uptime(&sys);
                println!();
                return;
            }
            "user"|"u" => {
                users(&sys);
                println!();
                return;
            }
            "help" => {
                help();
                println!();
                return;
            }
            _ => {
                continue;
            }
        }
    }
    println!("Command not found try help");
    help();
}

/// Initialisation des informations du système
fn init_sys_info() -> System {
    let mut sys = System::new_all();
    sys.refresh_all();
    sys
}
/// Memory fonction
fn memory(sys: &System) {
    println!("=== Memory ===");
    println!("total memory: {} Mbytes", sys.total_memory() / 1024 / 1024);
    println!("used memory : {} Mbytes", sys.used_memory() / 1024 / 1024);
    println!("total swap  : {} Mbytes", sys.total_swap() / 1024 / 1024);
    println!("used swap   : {} Mbytes", sys.used_swap() / 1024 / 1024);
    println!()
}
/// Cpu info
fn cpu(sys: &System) {
    for cpu in sys.cpus() {
        println!("{:?}", cpu);
    }
}
/// Uptime
fn uptime(sys: &System){
    let secondes = sys.uptime();
    let mintues = secondes / 60;
    let hours = mintues / 60;

    //retrancher les heures au mintues
    //retrancher les minutes au secondes
    if hours < 24
    {
        print!("uptime : {:?}h : {:?}m : {:?}s", hours,(mintues-(hours*60)), (secondes-(mintues*60)))
    };
    let days = hours / 24;
    print!("uptime : {:?}d : {:?}h : {:?}m : {:?}s",days, (hours-(days*24)),(mintues-(hours*60)), (secondes-(mintues*60)));
    println!()
}
/// users
fn users(sys: &System){
    for user in sys.users()  {
        println!();
        print!("name : {} - ",user.name());
        print!("uid : {:?} - ",user.id());
        print!("groups : {:?}",user.groups());
    }
    println!();
}
///Gpu temp
fn gpu_temp(sys: &System){
    for component in sys.components() {
        if component.label().contains("GPU")
        {println!("{}: {}°C", component.label(), component.temperature());}
    }
    println!();
} 
/// Help
fn help() {
    println!();
    println!("⌈⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺Help⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⌉");
    println!("⎪                                     ⎪");
    println!("⎪ Command            Available        ⎪");
    println!("⎪                                     ⎪");
    println!("⎪⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎪");
    println!("⎪                                     ⎪");
    println!("⎪ memory,m       =>    Memory info    ⎪");
    println!("⎪ cpu,c          =>    Cpu info       ⎪");
    println!("⎪ uptime,up       =>    Uptime info   ⎪");
    println!("⎪ user,u         =>    user list info ⎪");
    println!("⌊⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⌋");
    println!("");
}
